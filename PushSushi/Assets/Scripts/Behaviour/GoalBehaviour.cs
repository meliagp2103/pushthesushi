﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoalBehaviour : MonoBehaviour
{

    public int maxMove3Star;

    public static event Action<int> showMoveStars;

    public int maxMove2Star;

    private EndGamePage page;

    private int currentStarCount;


    private void OnEnable()
    {
        showMoveStars?.Invoke(maxMove3Star);
    }


    public void CheckForStar()
    {

        page = UIManager.Instance.endPage.GetComponent<EndGamePage>();

        switch (GameManager.Instance.MovesCount)
        {

            case int movesCount when movesCount <= maxMove3Star:
                {
                    //Debug.Log("Congrats 3 Stars");

                    //UIManager.Instance.endPage.GetComponent<EndGamePage>().starList.ForEach(star => star.sprite = UIManager.Instance.endPage.GetComponent<EndGamePage>().activeStar);

                    page.starList[0].sprite = page.activeStar;

                    page.starList[1].sprite = page.activeStar;

                    page.starList[2].sprite = page.activeStar;

                    currentStarCount = 3;

                    //SetInt("stars",3);

                    break;
                }

            case int movesCount when movesCount <= maxMove2Star:
                {
                    //Debug.Log("Congrats 2 Stars");

                    page.starList[0].sprite = page.activeStar;

                    page.starList[1].sprite = page.activeStar;

                    page.starList[2].sprite = page.disabledStar;

                    currentStarCount = 2;

                    //SetInt("stars",2);

                    break;
                }

            default:
                {
                    //Debug.Log("Congrats 1 Stars");

                    page.starList[0].sprite = page.activeStar;

                    page.starList[1].sprite = page.disabledStar;

                    page.starList[2].sprite = page.disabledStar;

                    currentStarCount = 1;

                    //SetInt("stars",1);

                    break;
                }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.Instance.ChangeState(GameState.EndGame);
            CheckForStar();

            if(GameManager.Instance.previousState == GameState.GameplayBonus)
            {
                return;
            }

            else
            {
                SaveData.LevelData currentLevel = new SaveData.LevelData();
                currentLevel.levelMoveCountCompleted = GameManager.Instance.MovesCount;
                currentLevel.levelStarCount = currentStarCount;

                List<SaveData.LevelData> levels = new List<SaveData.LevelData>(GameManager.Instance.saveData.levels);

                for (int i = 0; i < levels.Count; i++)
                {
                    if (i == SceneManager.GetActiveScene().buildIndex - 1)
                    {
                        SaveData.LevelData newRecordLevel = new SaveData.LevelData();
                        newRecordLevel.levelMoveCountCompleted = GameManager.Instance.saveData.levels[SceneManager.GetActiveScene().buildIndex - 1].levelMoveCountCompleted;
                        newRecordLevel.levelStarCount = GameManager.Instance.saveData.levels[SceneManager.GetActiveScene().buildIndex - 1].levelStarCount;

                        if (currentStarCount > newRecordLevel.levelStarCount)
                        {
                            newRecordLevel.levelStarCount = currentStarCount;
                        }

                        if (GameManager.Instance.MovesCount < newRecordLevel.levelMoveCountCompleted)
                        {
                            newRecordLevel.levelMoveCountCompleted = GameManager.Instance.MovesCount;
                        }

                        GameManager.Instance.saveData.levels[SceneManager.GetActiveScene().buildIndex - 1] = newRecordLevel;

                        GameManager.Instance.saveFile = JsonUtility.ToJson(GameManager.Instance.saveData);
                        File.WriteAllText(Application.persistentDataPath + "/SaveData.json", GameManager.Instance.saveFile);

                        //Debug.Log("Saving");

                        return;
                    }
                }

                GameManager.Instance.saveData.levels.Add(currentLevel);

                //Debug.Log("Saving" + GameManager.Instance.saveData);

                GameManager.Instance.CurrentLevelIndex++;

                GameManager.Instance.saveData.levelReached = GameManager.Instance.CurrentLevelIndex;

                GameManager.Instance.saveFile = JsonUtility.ToJson(GameManager.Instance.saveData);

                File.WriteAllText(Application.persistentDataPath + "/SaveData.json", GameManager.Instance.saveFile);
            }
            // Debug.Log("LEVEL COMPLETED");
        }
    }
}
