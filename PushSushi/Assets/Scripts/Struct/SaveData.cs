﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization;


[Serializable]
public class SaveData
{
    public List<LevelData> levels = new List<LevelData>();

    public int levelReached;

    public bool mute;

    [Serializable]
   public struct LevelData
   {
        public int levelStarCount;

        public int levelMoveCountCompleted;

   }
}
