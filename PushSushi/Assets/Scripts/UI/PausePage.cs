﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePage : MonoBehaviour
{ 
    public void Resume()
    {
        Debug.Log("Executed Resume");

        if (GameManager.Instance.previousState == GameState.GameplayBonus)
        {
            GameManager.Instance.ChangeState(GameState.GameplayBonus);
        }

        else
        {
            GameManager.Instance.ChangeState(GameState.Gameplay);
        }

    }

    public void ReturnToMenu()
    {
        Debug.Log("Executed Menus");
        GameManager.Instance.ChangeState(GameState.Menu);  
    }

    public void PlaySoundFx()
    {
        SoundManager.Instance.PlayFX("Button Sound", 0.5f);
    }

    public void ReloadLevel()
    {
        Debug.Log("Executed Reload");

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        GameManager.Instance.MovesCount = 0;

        if (GameManager.Instance.previousState == GameState.GameplayBonus)
        {
            //StartCoroutine(GameManager.Instance.Wait());
            GameManager.Instance.ChangeState(GameState.GameplayBonus);
        }

        else
        {
            //StartCoroutine(GameManager.Instance.Wait());
            GameManager.Instance.ChangeState(GameState.Gameplay);
        }
    }
}
