﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectionPage : MonoBehaviour
{

    public Sprite starActive;

    public Sprite starDisabled;

    public List<StarButton> buttons;

    private void OnEnable()
    {
        //Debug.Log(GameManager.Instance.saveData.levels[0].levelStarCount);

        for(int i = 0; i < buttons.Count; i++)
        {

            for(int j = 0; j < GameManager.Instance.saveData.levels.Count; j++)
            {
                if (j == i)
                {
                    switch (GameManager.Instance.saveData.levels[j].levelStarCount)
                    {
                        case 1:
                            {
                                buttons[i].star1.sprite = starActive;
                                buttons[i].star2.sprite = starDisabled;
                                buttons[i].star3.sprite = starDisabled;
                                break;
                            }
                        case 2:
                            {
                                buttons[i].star1.sprite = starActive;
                                buttons[i].star2.sprite = starActive;
                                buttons[i].star3.sprite = starDisabled;
                                break;
                            }
                        case 3:
                            {
                                buttons[i].star1.sprite = starActive;
                                buttons[i].star2.sprite = starActive;
                                buttons[i].star3.sprite = starActive;
                                break;
                            }

                    }

                    break;
                }
            }
           
        }
    }

    public void StartGame(int sceneID)
    {
        if(sceneID > GameManager.Instance.CurrentLevelIndex)
        {
            return;
        }

        else
        {
            GameManager.Instance.MovesCount = 0;
            //StartCoroutine(GameManager.Instance.Wait());
            GameManager.Instance.ChangeState(GameState.Gameplay);
            SceneManager.LoadScene(sceneID);
        }
        
    }

    public void PlaySoundFx()
    {
        SoundManager.Instance.PlayFX("Button Sound",0.5f);
    }

}
