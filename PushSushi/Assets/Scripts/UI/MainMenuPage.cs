﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuPage : MonoBehaviour
{
   public void LoadRandomLevel()
   {
        int randomIndex = Random.Range(1,15);

        Debug.Log(randomIndex);

        GameManager.Instance.ChangeState(GameState.GameplayBonus);

        GameManager.Instance.MovesCount = 0;

        SceneManager.LoadScene(randomIndex);
   }

    public void PlaySoundFx()
    {
        SoundManager.Instance.PlayFX("Button Sound", 0.5f);
    }

    private void OnEnable()
    {
        AdsManager.Instance.StartCoroutine(AdsManager.Instance.ActivateBanner());  
    }

}
