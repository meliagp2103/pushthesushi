﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class OptionPage : MonoBehaviour
{
    [HideInInspector]
    public bool mute;


    public void OnEnable()
    {
        mute = GameManager.Instance.saveData.mute;
    }


    public void PlaySoundFx()
    {
        SoundManager.Instance.PlayFX("Button Sound",0.5f);
    }

    public void Mute()
    {
        if (!mute)
        {
            mute = true;
            SoundManager.Instance.MuteSoundTrack();
        }
        else
        {
            mute = false;
            SoundManager.Instance.UnMuteSoundTrack();
        }
    }

    public void OnDisable()
    {
        GameManager.Instance.saveData.mute = mute;

        GameManager.Instance.saveFile = JsonUtility.ToJson(GameManager.Instance.saveData);

        File.WriteAllText(Application.persistentDataPath + "/SaveData.json", GameManager.Instance.saveFile);
    }


}
