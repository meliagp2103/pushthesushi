﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayPage : MonoBehaviour
{

    public TextMeshProUGUI moveCounterText;

    public TextMeshProUGUI starCounterText;

    public TextMeshProUGUI levelCounter;

    public TextMeshProUGUI BestMoveCounterText;

    public GameObject bestMovesPanel;

    public GameObject levelInfoPanel;

    private void OnEnable()
    {

        if(GameManager.Instance.currentState == GameState.GameplayBonus)
        {
            bestMovesPanel.SetActive(false);

            levelInfoPanel.SetActive(false);
        }

        else
        {

            bestMovesPanel.SetActive(true);

            levelInfoPanel.SetActive(true);

            GoalBehaviour.showMoveStars += ReceiveStarMove;

            for (int i = 0; i < GameManager.Instance.saveData.levels.Count; i++)
            {
                if (i == SceneManager.GetActiveScene().buildIndex - 1)
                {
                    BestMoveCounterText.text = GameManager.Instance.saveData.levels[i].levelMoveCountCompleted.ToString();
                    return;
                }
            }

            BestMoveCounterText.text = "0";
        }
    }

    // Update is called once per frame
    void Update()
    {
        moveCounterText.text = GameManager.Instance.MovesCount.ToString();

        GetLevlNumber();
    }


    public void Pause()
    {
        GameManager.Instance.ChangeState(GameState.Pause);
    }

    public void LoadNextLevel()
    {
        if (SceneManager.GetActiveScene().buildIndex + 1 < SceneManager.sceneCountInBuildSettings && SceneManager.GetActiveScene().buildIndex >= GameManager.Instance.CurrentLevelIndex)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

            GameManager.Instance.MovesCount = 0;

            GameManager.Instance.ChangeState(GameState.Gameplay);
        }
    }

    public void LoadPrevLevel()
    {
        if(SceneManager.GetActiveScene().buildIndex - 1 != 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);

            GameManager.Instance.MovesCount = 0;

            GameManager.Instance.ChangeState(GameState.Gameplay);
        }
        else
        {
            return;
        }
    }

    private void ReceiveStarMove(int starMove)
    {
        starCounterText.text = starMove.ToString();
    }

    public void GetLevlNumber()
    {
        levelCounter.text = SceneManager.GetActiveScene().buildIndex.ToString();
    }

    public void ResetLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        GameManager.Instance.MovesCount = 0;

        if(GameManager.Instance.previousState == GameState.GameplayBonus)
        {
            GameManager.Instance.ChangeState(GameState.GameplayBonus);
        }
        else
        {
            GameManager.Instance.ChangeState(GameState.Gameplay);
        }
    }

    public void PlaySoundFx()
    {
        SoundManager.Instance.PlayFX("Button Sound",0.5f);
    }

    private void OnDisable()
    {
        GoalBehaviour.showMoveStars -= ReceiveStarMove;
    }
}
