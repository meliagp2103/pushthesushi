﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndGamePage : MonoBehaviour
{

    public List<Image> starList;

    public Sprite activeStar;

    public Sprite disabledStar;

    public GameObject nextLevelButton;



    private void OnEnable()
    {
        if (GameManager.Instance.previousState == GameState.GameplayBonus)
        {
            nextLevelButton.SetActive(false);
        }

        else
        {
            nextLevelButton.SetActive(true);
        }
    }


    public void ReturnToMenu()
    {
        GameManager.Instance.ChangeState(GameState.Menu);
    }

    public void PlaySoundFx()
    {
        SoundManager.Instance.PlayFX("Button Sound", 0.5f);
    }

    public void ResetLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        GameManager.Instance.MovesCount = 0;

        if(GameManager.Instance.previousState == GameState.GameplayBonus)
        {
            GameManager.Instance.ChangeState(GameState.GameplayBonus);
        }
        else
        {
            GameManager.Instance.ChangeState(GameState.Gameplay);
        }

    }

    public void NextLevel()
    {
        if (SceneManager.GetActiveScene().buildIndex + 1 < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

            //to see better you moron
            if (SceneManager.GetActiveScene().buildIndex + 1 >= 2)
            {
                GameManager.Instance.MovesCount = -1;
            }

            else
            {
                GameManager.Instance.MovesCount = 0;
            }

            GameManager.Instance.ChangeState(GameState.Gameplay);

        }

        else
        {
            GameManager.Instance.ChangeState(GameState.Menu);
        }
    }


}
