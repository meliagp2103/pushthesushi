﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovable
{
    void Movement(Vector3 target,Vector2 delta);

    void Stop();
}
