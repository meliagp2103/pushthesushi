﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityBehaviour : MonoBehaviour, IMovable
{
    public enum Direction { Horizontal, Vertical }

    public Direction direction;

    public float speed;

    public float maxAcceleration;

    private float refSpeed;

    private Rigidbody rb;

    private bool canMove;

    private Vector3 targetPosition;

    private Vector3 delta;

    private Vector3 velocity;


    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Start()
    {
        rb.isKinematic = true;
    }

    public void Movement(Vector3 targetPosition, Vector2 deltaPosition)
    {
        rb.isKinematic = false;

        this.targetPosition = targetPosition;

        delta = new Vector3(deltaPosition.x,0,deltaPosition.y);

        this.targetPosition.y = 0;

        Vector2 target = new Vector2(this.targetPosition.x, targetPosition.z);

        refSpeed = speed;

        if (direction == Direction.Vertical)
        {
            float oldY = (target - deltaPosition).y;
            float newY = target.y;
            bool up = newY > oldY;
            canMove = (target.y > rb.position.z && up) || (target.y < rb.position.z && !up);
        }
        else
        {
            float oldX = (target - deltaPosition).x;
            float newX = target.x;
            bool right = newX > oldX;
            canMove = (target.x > rb.position.x && right) || (target.x < rb.position.x && !right);
        }
    }

    private void FixedUpdate()
    {
        if (canMove)
            rb.MovePosition(rb.position + Vector3.ClampMagnitude(delta,maxAcceleration) * refSpeed * Time.fixedDeltaTime);

        //rb.MovePosition(rb.position + targetPosition * speed * Time.fixedDeltaTime);

        //rb.MovePosition(Vector3.Lerp(rb.position, targetPosition, Time.fixedDeltaTime * speed));

        //rb.MovePosition(targetPosition * Time.fixedDeltaTime * speed);
        //rb.position = Vector3.SmoothDamp(rb.position, targetPosition, ref velocity, duration);
    }

    public void Stop()
    {

        GameManager.Instance.MovesCount++;

        SoundManager.Instance.PlayFX("Drag Sound", 0.5f);

        refSpeed = 0;

        rb.position = new Vector3(Mathf.Round(rb.position.x), 0, Mathf.Round(rb.position.z));

        rb.isKinematic = true;

    }
}