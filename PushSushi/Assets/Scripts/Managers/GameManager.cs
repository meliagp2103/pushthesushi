﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public enum GameState { Menu, Gameplay, GameplayBonus ,EndGame ,Pause }

public class GameManager : MonoBehaviour
{

    public GameState currentState;

    public GameState previousState;

    public event Action<GameState> showCurrentPage;

    public static GameManager Instance;

    public SaveData saveData = new SaveData();

    [HideInInspector]
    public string saveFile;

    private int movesCount;

    private int currentLevelIndex = 1;

    public int MovesCount { get { return movesCount; } set { movesCount = value; } }

    public int CurrentLevelIndex { get { return currentLevelIndex; } set { currentLevelIndex = value; } }


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        else
        {
            Destroy(this);
        }
    }


    public void ChangeState(GameState nextGameState)
    {
        previousState = currentState;

        currentState = nextGameState;

        showCurrentPage?.Invoke(currentState);
    }

    // Start is called before the first frame update
    void Start()
    {
        currentState = GameState.Menu;

        if (File.Exists(Application.persistentDataPath + "/SaveData.json"))
        {
            saveFile = File.ReadAllText(Application.persistentDataPath + "/SaveData.json");
            saveData = JsonUtility.FromJson<SaveData>(saveFile);
            CurrentLevelIndex = saveData.levelReached;

            //preduiiction of possible save Explosion
            if(CurrentLevelIndex == 0)
            {
                CurrentLevelIndex = 1;
            }
        }

        showCurrentPage?.Invoke(currentState);
    }
}
