﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    public static InputManager Instance;

    public float swipeDelay;

    private IMovable target;

    private Vector2 beginPosition;

    private Vector2 swipeDirection;

    private float timer;


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if(GameManager.Instance.currentState == GameState.Gameplay || GameManager.Instance.currentState == GameState.GameplayBonus)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                switch (touch.phase)
                {
                    case TouchPhase.Began:

                        beginPosition = touch.position;

                        CheckForTarget(touch.position);

                        break;

                    case TouchPhase.Moved:

                        Ray ray = Camera.main.ScreenPointToRay(touch.position);

                        if (Physics.Raycast(ray, out RaycastHit hit))
                        {
                            target?.Movement(hit.point, touch.deltaPosition);
                        }

                        break;
                    case TouchPhase.Stationary:
                        break;
                    case TouchPhase.Ended:

                        if (target != null)
                        {
                            if(touch.position != beginPosition)
                            {
                                target?.Stop();
                            }

                            target = null;
                        }

                        break;
                    case TouchPhase.Canceled:
                        break;
                }
            }
        }
    }

    public void CheckForTarget(Vector2 touchPosition)
    {
        Ray ray = Camera.main.ScreenPointToRay(touchPosition);

        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            target = hit.collider.GetComponent<IMovable>();
            Debug.Log(target);
            //IMovable movable = hit.collider.GetComponent<IMovable>();
            //target = movable != null ? movable : target;
        }
    }
}