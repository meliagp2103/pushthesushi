﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    public static UIManager Instance;

    public GameObject mainMenuPage;

    public GameObject levelSelectionPage;

    public GameObject gameplayPage;

    public GameObject pausePage;

    public GameObject endPage;


    private void Awake()
    {
        if(!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        else
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.showCurrentPage += OnShowCurrentPage;
    }

    public void OnShowCurrentPage(GameState state)
    {
        switch (state)
        {
            case GameState.Menu:
                if (GameManager.Instance.previousState == GameState.EndGame || GameManager.Instance.previousState == GameState.Pause)
                {
                    mainMenuPage.SetActive(false);
                    levelSelectionPage.SetActive(true);
                    gameplayPage.SetActive(false);
                    pausePage.SetActive(false);
                    endPage.SetActive(false);
                }
                else
                {
                    mainMenuPage.SetActive(true);
                    levelSelectionPage.SetActive(false);
                    gameplayPage.SetActive(false);
                    pausePage.SetActive(false);
                    endPage.SetActive(false);
                }
                break;

            case GameState.Gameplay:
                mainMenuPage.SetActive(false);
                levelSelectionPage.SetActive(false);
                gameplayPage.SetActive(true);
                pausePage.SetActive(false);
                endPage.SetActive(false);
                break;

            case GameState.GameplayBonus:
                mainMenuPage.SetActive(false);
                levelSelectionPage.SetActive(false);
                gameplayPage.SetActive(true);
                pausePage.SetActive(false);
                endPage.SetActive(false);
                break;

            case GameState.EndGame:
                mainMenuPage.SetActive(false);
                levelSelectionPage.SetActive(false);
                gameplayPage.SetActive(false);
                pausePage.SetActive(false);
                endPage.SetActive(true);
                break;

            case GameState.Pause:
                mainMenuPage.SetActive(false);
                levelSelectionPage.SetActive(false);
                gameplayPage.SetActive(true);
                pausePage.SetActive(true);
                endPage.SetActive(false);
                break;
        }
    }

    //// Update is called once per frame
    //void Update()
    //{
        
    //}
}
