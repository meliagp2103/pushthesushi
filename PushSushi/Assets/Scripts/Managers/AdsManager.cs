﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour,IUnityAdsListener
{

    public static AdsManager Instance;

    public readonly string videoPlacement = "video";

    public readonly string rewardVideo = "rewardedVideo";

    public readonly string banner = "banner";

    [HideInInspector]
    public bool adStarted;

    [HideInInspector]
    public bool adCompleted;

    public bool isTestMode = true;
    


    private string gameIDAndroid = "3924255";

    private string gameIDIos = "3924254";


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }

        else
        {
            Destroy(this);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        Advertisement.AddListener(this);

        InitializeAdsPlatform();
    }

    private void InitializeAdsPlatform()
    {
        #if UNITY_ANDROID || UNITY_EDITOR
        Advertisement.Initialize(gameIDAndroid, isTestMode);
        #endif

        #if UNITY_IOS
        Advertisement.Initialize(gameIDIos, isTestMode);
        #endif
    }

    public void ShowAd(bool isReward)
    {
        string adType = isReward ? rewardVideo : videoPlacement;

        if(Advertisement.isInitialized && !adStarted && Advertisement.IsReady(adType))
        {
            Advertisement.Show(adType);
            adStarted = true;
        }
        else
        {
            Debug.LogError("ERROR: The advertisement selected inside the ShowAd Method is not Ready or Initialized");
        }
    }

    public IEnumerator ActivateBanner()
    {
        while (!Advertisement.isInitialized)
        {
            yield return new WaitForEndOfFrame();
        }

        while (!Advertisement.IsReady(banner))
        {
            yield return new WaitForEndOfFrame();
        }

        Advertisement.Banner.SetPosition(BannerPosition.TOP_LEFT);

        Advertisement.Banner.Show(banner);

        while (UIManager.Instance.mainMenuPage.activeSelf)
        {
            yield return new WaitForEndOfFrame();
        }

        Advertisement.Banner.Hide();

        yield return null;
    }

    public void OnUnityAdsReady(string placementId)
    {
        print("Ready " + placementId);
    }

    public void OnUnityAdsDidError(string message)
    {
        Debug.LogError("ERROR: The ads did Error");
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        Debug.Log("Started: " + placementId);
        if (placementId == rewardVideo)
            UIManager.Instance.mainMenuPage.SetActive(false);
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        Debug.Log("Finished: " + placementId + "Status: " + showResult);

        adStarted = false;

        if (placementId == rewardVideo)
            UIManager.Instance.mainMenuPage.SetActive(true);

        adCompleted = showResult == ShowResult.Finished;
    }
}
