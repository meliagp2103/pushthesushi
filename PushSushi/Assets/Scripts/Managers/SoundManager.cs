﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;

    private AudioSource source;

    public AudioSource soundtrackSource;

    [SerializeField] private List<AudioClip> fx;

    [SerializeField] private List<AudioClip> soundTracks;

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            source = GetComponent<AudioSource>();
        }
        else
        {
            Destroy(this);
        }
    }

    public void PlayFX(string clipName, float vol = 1, float pitch = 1)
    {
        source.pitch = pitch;

        source.PlayOneShot(fx.Single(clip => clip.name.Equals(clipName)), vol);
    }

    public void PlaySoundtrack()
    {
        soundtrackSource.clip = soundTracks[0];

        soundtrackSource.Play();
    }

    public void MuteSoundTrack()
    {
        soundtrackSource.mute = true;
    }

    public void UnMuteSoundTrack()
    {
        soundtrackSource.mute = false;
    }

    public void Stop()
    {
        source.Stop();
    }
}
